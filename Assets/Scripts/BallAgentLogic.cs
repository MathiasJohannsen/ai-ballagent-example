﻿using UnityEngine;
using Unity.MLAgents;
using Unity.MLAgents.Sensors;

public class BallAgentLogic : Agent
{
    Rigidbody rBody;

    void Start()
    {
        rBody = GetComponent<Rigidbody>();
    }

    public Transform target;
    public override void OnEpisodeBegin()
    {
        // Reset Agent
        this.rBody.angularVelocity = Vector3.zero;
        this.rBody.velocity = Vector3.zero;
        this.transform.localPosition = new Vector3(-9, 0.5f, 0);

        // Move target to a new position
        target.localPosition = new Vector3(12 + Random.value * 8, Random.value * 3, Random.value * 10 - 5);
    }

    public override void CollectObservations(VectorSensor sensor)
    {
        sensor.AddObservation(target.localPosition);
        sensor.AddObservation(this.transform.localPosition);
        sensor.AddObservation(rBody.velocity);
    }

    public float speed = 20f;
    public override void OnActionReceived(float[] vectorAction)
    {
        Vector3 controllSignal = Vector3.zero;
        controllSignal.x = vectorAction[0];

        //mapping the output:
        // 0 ->  0 ... do nothing
        // 1 -> -1 ... turn left or right
        // 2 ->  1 ... turn other direction
        if (vectorAction[1] == 2)
        {
            controllSignal.z = 1;
        }
        else
        {
            controllSignal.z = -vectorAction[1];
        }

        // Prevent adding forces after jumping:
        if (this.transform.localPosition.x < 8.5)
        {
            rBody.AddForce(controllSignal * speed);
        }

        float distanceToTarget = Vector3.Distance(this.transform.localPosition, target.localPosition);
        // Target reached
        if (distanceToTarget < 1.42f)
        {
            SetReward(1);
            EndEpisode();
        }

        // Fell off platform
        if (this.transform.localPosition.y < 0)
        {
            EndEpisode();
        }
    }

    //just for testing the environment:
    public override void Heuristic(float[] actionsOut)
    {
        actionsOut[0] = Input.GetAxis("Vertical");
        actionsOut[1] = Input.GetAxis("Horizontal");
    }
}
