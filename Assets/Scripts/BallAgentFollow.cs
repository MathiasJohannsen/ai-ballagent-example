﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallAgentFollow : MonoBehaviour
{
    public Transform BallAgentTransform;

    Vector3 cameraOffset;

    void Start()
    {
        cameraOffset = transform.position - BallAgentTransform.position;
    }

    void LateUpdate()
    {
        transform.position = BallAgentTransform.position + cameraOffset;
    }
}
